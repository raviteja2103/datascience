# -*- coding: utf-8 -*-
"""
Created on Wed Jun  10 22:05:23 2015

@author: raviteja
"""

import getopt
import pandas as pd
import numpy as np
import os, sys
import string
import csv
from __future__ import with_statement
import gc
import re
import os,sys
import logging
from dateutil import parser
import ConfigParser
from datetime import datetime


import networkx as nx
import matplotlib.pyplot as plt
import community

from ayasdi.core.api import Api
from ayasdi.core.models import create_model, apply_model

def main():

    # parse command line options
    try:
        dflist = listframes()
    except Exception as ex:
        print ex.message
        return 1 # indicates error, but not necessary
    else:
        return 0 # return 0 # indicates errorlessly exit, but not necessary

    ######## Logging  #########
    timestr = time.strftime("%Y%m%d-%H%M%S")
    file_name = 'mercy_denials.log-' + timestr + '.log'
    logging.basicConfig(filename=file_name, level=logging.INFO)
    logger = logging.getLogger() 


def connection():
    #API
    connection = Api()    

def create_logger(filename): 
    '''Logging instead of print
    '''
    logger = logging.getLogger()
    file_handle = logging.FileHandler(os.path.join(filename))
    sout_handle = logging.StreamHandler()
    # type of log
    logger.setLevel(logging.INFO); file_handle.setLevel(logging.INFO)
    sout_handle.setLevel(logging.INFO)
    #format
    f = logging.Formatter('%(asctime)s - %(message)s')
    file_handle.setFormatter(f); sout_handle.setFormatter(f)
    logger.addHandler(file_handle);logger.addHandler(sout_handle)
    return logger



def upload_source(filename):
    '''Verifies and uploads a csv file to Ayasdi core
    '''
    try:
        connection.delete_source(name=filename)
    except IndexError:
        source = connection.upload_source(filename)
    return source


def generate_columnsets(source, column_list):
	''' Pass the list of column numbers to select columns used to build network
	'''
	#Coloring based on rows per node
	coloring = source.get_coloring(name="Rows per Node")

	#Creating a column set
	col_set = source.create_column_set(column_list, "cs")

	return col_set, coloring


def auto_analysis(source, col_set):

    ''' Pass a column set and get autoanalyssis suggestions to build a network
    '''
    suggestions =  source.get_auto_analysis_suggestions(col_set)

    source.sync()

    logging.info("The source has %s networks." % len(source.get_networks()))
    logging.info("The suggestions are %s" %(suggestions))

    return suggestions


def build_auto_network(suggestions, source):

    ''' Build a network based on the suggestions provided(passed)
    '''
    running_jobs = []
    # Creating network based on suggestions
    for ind, suggestion in enumerate(suggestions):
        network_name = 'Analysis %s' % ind
        print "Starting %s." % network_name
        new_job = source.create_network(network_name, suggestion)
        running_jobs.append(new_job)

    logging.info('Running jobs are %s' %(running_jobs))


def custom_networks(source, metric, lens, col_set, res, gain, equalize):

    '''Custom network built on Column Set A and CS B. Suggestions didnt create a network using L infinity centrality
    '''

    network = source.create_network("Custom_Network",{
        'metric': {'id': metric}, 
        'column_set_id': col_set['id'], 
        'lenses': [{'resolution': res, 'id': lens, 
                    'equalize': equalize, 'gain': gain}] 
        } 
        )

    return network

def auto_grouping(network, cut_off, column_name):
    ''' Auto grouping based on community detection algorithm
    '''
    coloring = source.create_coloring(name='%s_coloring' %(column_name),
                                      column_name=column_name)
    coloring_values = network.get_coloring_values(name='%s_coloring' %(column_name))
    autogroups = network.autogroup(cutoff_strength=cut_off,
                               coloring_values=coloring_values)
    for ind, autogroup in enumerate(autogroups):
        network.create_node_group(name="Autogroup%s" % ind,
                               nodes=autogroup)
    # Comparison of groups
    # comparison = network.compare_groups("Autogroup0", "Autogroup1")
    # return comparison


if __name__ == '__main__':
    main()
